CREATE TABLE receipe.user (
	userId INT AUTO_INCREMENT PRIMARY KEY,
    ownerId INT,
    username varchar(50) NOT NULL,
	fullName VARCHAR(120) NOT NULL,
	email VARCHAR(120) NOT NULL,
    pass VARCHAR(30) NOT NULL,
    createdAt DATETIME NOT NULL,
    updatedAt DATETIME,
    deletedAt DATETIME
);

INSERT INTO user (userId, userName, fullName, email, pass, createdAt)
VALUES (1, "Lan", "Truong", "lan@", "123", 12/12/2000);