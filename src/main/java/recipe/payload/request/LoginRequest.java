package recipe.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import static recipe.constants.jsonRequest.JsonLoginConstants.*;

@Getter
@Setter
public class LoginRequest {
    @JsonProperty(USERNAME)
    private String username;

    @JsonProperty(PASSWORD)
    private String password;
}
