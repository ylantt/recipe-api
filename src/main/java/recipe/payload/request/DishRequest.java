package recipe.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class DishRequest {
    private UUID owner;

    private String name;

    private String dishDesc;

    private String mediaUrl;

    private String mediaDesc;

    private UUID categoryId;

    private List<DishIngredientRequest> ingredients;
}
