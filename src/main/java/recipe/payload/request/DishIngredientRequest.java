package recipe.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class DishIngredientRequest {

    private UUID ingredientId;

    private int amount;

    private String unit;
}
