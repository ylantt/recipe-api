package recipe.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import static recipe.constants.jsonRequest.JsonSignupConstants.*;

@Getter
@Setter
public class SignupRequest {
    @JsonProperty(USERNAME)
    private String username;

    @JsonProperty(FULL_NAME)
    private String fullName;

    @JsonProperty(EMAIL)
    private String email;

    @JsonProperty(PASSWORD)
    private String password;
}
