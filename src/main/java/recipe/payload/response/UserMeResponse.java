package recipe.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserMeResponse {
    private String username;
    private String email;
}
