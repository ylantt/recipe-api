package recipe.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class DishIngredientResponse {
    private UUID ingredientId;

    private int amount;

    private String unit;
}
