package recipe.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class DishResponse {
    private UUID id;

    private UUID owner;

    private String name;

    private String dishDesc;

    private String mediaUrl;

    private String mediaDesc;

    private CategoryResponse category;

    private List<DishIngredientResponse> ingredients;
}
