package recipe.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IngredientResponse {
    private String name;
}
