package recipe.config.env;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@Getter
@Setter
@ConfigurationProperties(prefix = "token")
public class JwtENV {
    private String name;
    private String secret;
    private int validity;
}
