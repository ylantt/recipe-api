package recipe.config.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import recipe.DTO.UserDetailsImpl;
import recipe.services.impl.UserDetailsServiceImpl;
import recipe.utils.JwtTokenUtil;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

import static recipe.constants.SecurityConstants.HEADER_STRING;
import static recipe.constants.SecurityConstants.TOKEN_PREFIX;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
        throws ServletException, IOException {
        final String requestTokenHeader = request.getHeader(HEADER_STRING);

        if (requestTokenHeader == null || !requestTokenHeader.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        String tokenValue = requestTokenHeader.replace(TOKEN_PREFIX, "");

        UsernamePasswordAuthenticationToken authentication = getAuthentication(tokenValue);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {
        UserDetailsImpl userDetails = jwtTokenUtil.parseToken(token);

        if (token != null && userDetails != null) {
            Collection<? extends GrantedAuthority> grantedAuthorities
                    = userDetailsService.buildAuthorities(userDetails.getRoleNameList());

            userDetails.setAuthorities(grantedAuthorities);

            return new UsernamePasswordAuthenticationToken(
                    userDetails.getUsername(),
                    userDetails.getPassword(),
                    grantedAuthorities);
        }

        return null;
    }
}
