package recipe.utils;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import recipe.DTO.UserDetailsImpl;
import recipe.config.env.JwtENV;
import static recipe.constants.UserPropertyConstants.*;
import static recipe.constants.responseMessages.errorResponses.JWTResponses.*;

import java.util.*;

@Component
public class JwtTokenUtil {
    @Autowired
    private JwtENV jwtENV;

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class);

    public String generateToken(String username, List<String> roles) {
        Map<String, Object> claims = new HashMap<>();

        return Jwts.builder().setClaims(claims).setSubject(username)
                .claim(ROLES_PROPERTY, roles)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + jwtENV.getValidity()))
                .signWith(SignatureAlgorithm.HS512, jwtENV.getSecret()).compact();
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(jwtENV.getSecret()).parseClaimsJws(token).getBody();
    }

    public UserDetailsImpl parseToken(String authToken) {
        try {
            Claims body = getAllClaimsFromToken(authToken);

            UserDetailsImpl u = new UserDetailsImpl();
            u.setUsername(body.getSubject());
            u.setRoleNameList((ArrayList<String>) body.get(ROLES_PROPERTY));

            return u;
        } catch (SignatureException e) {
            logger.error(JWT_SIGNATURE_INVALID, e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error(JWT_TOKEN_INVALID, e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error(TOKEN_EXPIRED, e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error(JWT_TOKEN_UNSUPPORTED, e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error(JWT_CLAIMS_EMPTY, e.getMessage());
        }
        return null;
    }
}
