package recipe.utils;

import java.sql.Timestamp;

public class GeneralUtil {
    private GeneralUtil() {
        throw new UnsupportedOperationException();
    }

    public static Long getCurrentTimestamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp.getTime();
    }
}
