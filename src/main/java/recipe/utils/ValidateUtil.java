package recipe.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ValidateUtil {
    // Private constructor to prevent instantiation
    private ValidateUtil() {
        throw new UnsupportedOperationException();
    }

    //public static methods here
    public static boolean checkMatchStringByRegex(String regex, String checkTarget) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(checkTarget);
        return matcher.matches();
    }
}
