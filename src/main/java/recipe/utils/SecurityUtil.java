package recipe.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public final class SecurityUtil {
    private SecurityUtil() {
        throw new UnsupportedOperationException();
    }

    public static String encodePasswordBCrypt(String rawPass) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(rawPass);
    }
}
