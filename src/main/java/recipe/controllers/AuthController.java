package recipe.controllers;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import recipe.DTO.UserDTO;
import recipe.payload.request.LoginRequest;
import recipe.payload.request.SignupRequest;
import recipe.payload.response.LoginResponse;
import recipe.payload.response.UserMeResponse;
import recipe.services.AuthService;

import static recipe.constants.ResponseCodeConstants.SUCCESS_MESSAGE;
import static recipe.constants.routes.RouteAuthConstants.*;

@RestController
@RequestMapping(GENERAL_AUTH_ROUTE)
public class AuthController {
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private AuthService authService;

    @PostMapping(SIGN_UP_ROUTE)
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signupRequest) {
        UserDTO userDTO = modelMapper.map(signupRequest, UserDTO.class);

        String responseMessage = authService.signup(userDTO);

        if (responseMessage != SUCCESS_MESSAGE) {
            return new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(responseMessage, HttpStatus.CREATED);
    }

    @PostMapping(SIGN_IN_ROUTE)
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        UserDTO userDTO = modelMapper.map(loginRequest, UserDTO.class);

        LoginResponse loginResponse = authService.signin(userDTO);

        return ResponseEntity.ok()
                .body(loginResponse);
    }

    @GetMapping(ME_ROUTE)
    public ResponseEntity<?> getMe() {
        UserDTO userDTO = authService.getMe();

        UserMeResponse userMeResponse = modelMapper.map(userDTO, UserMeResponse.class);

        return ResponseEntity.ok()
                .body(userMeResponse);
    }
}