package recipe.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import recipe.DTO.IngredientDTO;
import recipe.payload.request.IngredientRequest;
import recipe.payload.response.IngredientResponse;
import recipe.services.IngredientService;

import javax.validation.Valid;

import static recipe.constants.routes.RouteIngredientConstants.*;

@RestController
@RequestMapping(GENERAL_INGREDIENT_ROUTE)
public class IngredientController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IngredientService ingredientService;

    @PostMapping("/")
    public ResponseEntity<IngredientResponse> createDish(@Valid @RequestBody IngredientRequest ingredientRequest) {
        IngredientDTO ingredientDTO = modelMapper.map(ingredientRequest, IngredientDTO.class);

        IngredientDTO newIngredient = ingredientService.create(ingredientDTO);

        return new ResponseEntity<>(
                modelMapper.map(newIngredient, IngredientResponse.class),
                HttpStatus.CREATED);
    }
}
