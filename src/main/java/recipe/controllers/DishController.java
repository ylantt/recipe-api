package recipe.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import recipe.DTO.DishDTO;
import recipe.payload.request.DishRequest;
import recipe.payload.response.DishResponse;
import recipe.services.DishService;

import javax.validation.Valid;

import java.util.UUID;

import static recipe.constants.routes.RouteDishConstants.GENERAL_DISH_ROUTE;

@RestController
@RequestMapping(GENERAL_DISH_ROUTE)
public class DishController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DishService dishService;

    @PostMapping("/")
    public ResponseEntity<DishResponse> createDish(@Valid @RequestBody DishRequest dishRequest) {
        // map categoryId request to categoryDTO's id
        modelMapper.typeMap(DishRequest.class, DishDTO.class)
                    .addMappings(mapper -> mapper.<UUID>map(
                                    src -> dishRequest.getCategoryId(),
                                    (dest, v) -> dest.getCategory().setId(v)));

        DishDTO dishDTO = modelMapper.map(dishRequest, DishDTO.class);

        DishDTO newDish = dishService.create(dishDTO);

        return new ResponseEntity<>(
                modelMapper.map(newDish, DishResponse.class),
                HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteDish(@PathVariable UUID id) {
        dishService.delete(id);
        return new ResponseEntity<>(
                "delete success",
                HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DishResponse> getDish(@PathVariable UUID id) {
        DishDTO dishDTO = dishService.get(id);

        return new ResponseEntity<>(
                modelMapper.map(dishDTO, DishResponse.class),
                HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateDish(@Valid @RequestBody DishRequest dishRequest, @PathVariable UUID id) {
        modelMapper.typeMap(DishRequest.class, DishDTO.class)
                .addMappings(mapper -> mapper.<UUID>map(
                        src -> dishRequest.getCategoryId(),
                        (dest, v) -> dest.getCategory().setId(v)));

        DishDTO dishDTO = modelMapper.map(dishRequest, DishDTO.class);
        dishDTO.setId(id);

        DishDTO updatedDishDTO = dishService.update(dishDTO);

        return new ResponseEntity<>(
                modelMapper.map(updatedDishDTO, DishResponse.class),
                HttpStatus.OK);
    }
}
