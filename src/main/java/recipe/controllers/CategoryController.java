package recipe.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import recipe.DTO.CategoryDTO;
import recipe.payload.request.CategoryRequest;
import recipe.payload.response.CategoryResponse;
import recipe.services.CategoryService;

import javax.validation.Valid;

import static recipe.constants.routes.RouteCategoryConstants.*;

@RestController
@RequestMapping(GENERAL_CATEGORY_ROUTE)
public class CategoryController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/")
    public ResponseEntity<CategoryResponse> createDish(@Valid @RequestBody CategoryRequest categoryRequest) {
        CategoryDTO categoryDTO = modelMapper.map(categoryRequest, CategoryDTO.class);

        CategoryDTO newCategory = categoryService.create(categoryDTO);

        return new ResponseEntity<>(
                modelMapper.map(newCategory, CategoryResponse.class),
                HttpStatus.CREATED);
    }
}
