package recipe.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class IngredientDTO {
    private UUID id;
    private String name;
}
