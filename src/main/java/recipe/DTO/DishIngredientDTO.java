package recipe.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;


@Getter
@Setter
public class DishIngredientDTO {
    private UUID dishId;

    private UUID ingredientId;

    private int amount;

    private String unit;
}
