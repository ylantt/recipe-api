package recipe.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class CategoryDTO {
    private UUID id;

    private String name;

    private String cateDescription;

    private List<DishDTO> dishes;

    public CategoryDTO() {}

    public CategoryDTO(UUID id, String name, String cateDescription) {
        this.id = id;
        this.name = name;
        this.cateDescription = cateDescription;
    }
}
