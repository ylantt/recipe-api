package recipe.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class UserDTO {
    private UUID id;

    private String username;

    private String fullName;

    private String email;

    private String password;

    private Long createdAt;

    Set<RoleDTO> roles = new HashSet<RoleDTO>();
}