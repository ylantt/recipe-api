package recipe.DTO;

import lombok.Getter;
import lombok.Setter;
import recipe.enums.RoleEnum;

import java.util.*;

@Getter
@Setter
public class RoleDTO {
    private UUID id;
    private RoleEnum name;
    Set<UserDTO> users = new HashSet<UserDTO>();
}
