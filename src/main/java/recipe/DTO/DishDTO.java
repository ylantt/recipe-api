package recipe.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class DishDTO {
    private UUID id;

    private UUID owner;

    private String name;

    private String dishDesc;

    private double avgRating;

    private int totalCmt;

    private String mediaUrl;

    private String mediaDesc;

    private CategoryDTO category;

    private List<DishIngredientDTO> ingredients;

    private Long createdAt;

    private Long updatedAt;

    private Long deletedAt;
}
