package recipe.repositories;

import recipe.entities.UserEntity;

public interface UserRepositoryCustom {
    public UserEntity saveNew(UserEntity userEntity);
}
