package recipe.repositories.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import recipe.entities.RoleEntity;
import recipe.entities.UserEntity;
import recipe.repositories.RoleRepository;
import recipe.repositories.UserRepository;
import recipe.repositories.UserRepositoryCustom;

import static recipe.enums.RoleEnum.ROLE_USER;

@Repository
public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserEntity saveNew(UserEntity userEntity) {
        RoleEntity roleEntity = roleRepository.findByName(ROLE_USER);

        if (roleEntity == null) {
            RoleEntity newRole = new RoleEntity(ROLE_USER);
            userEntity.addRole(newRole);
        } else {
            userEntity.addRole(roleEntity);
        }

        userRepository.save(userEntity);

        return userEntity;
    }
}
