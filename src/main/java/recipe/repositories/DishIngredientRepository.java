package recipe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import recipe.entities.DishIngredient;
import recipe.entities.compositeId.DishIngredientID;

import java.util.List;
import java.util.UUID;

@Repository
public interface DishIngredientRepository extends JpaRepository<DishIngredient, DishIngredientID> {
    List<DishIngredient> findByDishId(UUID dishId);
}
