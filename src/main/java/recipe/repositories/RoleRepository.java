package recipe.repositories;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import recipe.entities.RoleEntity;
import recipe.enums.RoleEnum;
import java.util.List;
import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, UUID> {
    RoleEntity findByName(RoleEnum name);

    @Override
    List<RoleEntity> findAll(Sort sort);
}