package recipe.services;

import recipe.DTO.CategoryDTO;

import java.util.UUID;

public interface CategoryService {
    CategoryDTO create(CategoryDTO categoryDTO);
    CategoryDTO get(UUID cateId);
}