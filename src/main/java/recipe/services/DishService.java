package recipe.services;

import recipe.DTO.DishDTO;

import java.util.UUID;

public interface DishService {
    DishDTO create(DishDTO dishDTO);
    void delete(UUID id);
    DishDTO get(UUID dishId);
    DishDTO update(DishDTO dishDTO);
}
