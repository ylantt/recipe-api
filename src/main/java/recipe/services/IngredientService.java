package recipe.services;

import recipe.DTO.IngredientDTO;

public interface IngredientService {
    IngredientDTO create(IngredientDTO ingredientDTO);
}
