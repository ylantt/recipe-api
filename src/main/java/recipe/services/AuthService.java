package recipe.services;

import recipe.DTO.UserDTO;
import recipe.payload.response.LoginResponse;

public interface AuthService {
    String signup(UserDTO userDTO);
    LoginResponse signin(UserDTO userDTO);
    UserDTO getMe();
}
