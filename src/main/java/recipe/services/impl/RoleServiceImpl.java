package recipe.services.impl;

import org.springframework.stereotype.Service;
import recipe.DTO.RoleDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {
    public List<String> getRoleNameListFromRoleDTO(Set<RoleDTO> roleDTOs) {
        List<String> roleNameList = new ArrayList<String>();
        for (RoleDTO roleDTO : roleDTOs) {
            roleNameList.add(roleDTO.getName().name());
        }
        return roleNameList;
    }
}
