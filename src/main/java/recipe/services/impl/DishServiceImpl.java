package recipe.services.impl;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import recipe.DTO.CategoryDTO;
import recipe.DTO.DishDTO;
import recipe.DTO.DishIngredientDTO;
import recipe.entities.Dish;
import recipe.entities.DishIngredient;
import recipe.repositories.DishIngredientRepository;
import recipe.repositories.DishRepository;
import recipe.services.CategoryService;
import recipe.services.DishIngredientService;
import recipe.services.DishService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DishServiceImpl implements DishService {

    @Autowired
    private DishIngredientService dishIngredientService;

    @Autowired
    private DishRepository dishRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DishIngredientRepository dishIngredientRepository;

    @Autowired
    private ModelMapper modelMapper;

    public DishDTO create(DishDTO dishDTO) {
        dishDTO.setAvgRating(0);
        dishDTO.setTotalCmt(0);
        DishDTO newDish = saveToDb(dishDTO);
        List<DishIngredientDTO> dishIngredientDTOList = dishIngredientService.create(dishDTO.getIngredients(), newDish.getId());
        newDish.setIngredients(dishIngredientDTOList);
        return newDish;
    }

    private DishDTO saveToDb(DishDTO dishDTO) {
        modelMapper.typeMap(DishDTO.class, Dish.class)
                .addMappings(mapper -> mapper.<UUID>map(
                        src -> dishDTO.getCategory().getId(),
                        Dish::setCategoryId));

        Dish dish = modelMapper.map(dishDTO, Dish.class);

        Dish newDish = dishRepository.save(dish);
        return modelMapper.map(newDish, DishDTO.class);
    }

    public void delete(UUID id) {
        dishRepository.deleteById(id);
    }

    public DishDTO get(UUID dishId) {
        DishDTO dishDTO = getMainData(dishId);
        if (dishDTO == null) {
            return null;
        }

        CategoryDTO categoryDTO = categoryService.get(dishDTO.getCategory().getId());
        if (categoryDTO == null) {
            return null;
        }

        CategoryDTO category = new CategoryDTO(categoryDTO.getId(), categoryDTO.getName(), categoryDTO.getCateDescription());

        dishDTO.setCategory(category);

        List<DishIngredient> dishIngredients = dishIngredientRepository.findByDishId(dishDTO.getId());

        dishDTO.setIngredients(modelMapper.map(dishIngredients, new TypeToken<List<DishIngredientDTO>>() {}.getType()));

        return dishDTO;
    }

    private DishDTO getMainData(UUID dishId) {
        Optional<Dish> dish = dishRepository.findById(dishId);
        return dish.map(value -> modelMapper.map(value, DishDTO.class)).orElse(null);
    }

    public DishDTO update(DishDTO dishDTO) {
        DishDTO verifiedDishDTO = getMainData(dishDTO.getId());

        if (verifiedDishDTO == null) {
            return null;
        }

        dishDTO.setCreatedAt(verifiedDishDTO.getCreatedAt());

        DishDTO newDish = saveToDb(dishDTO);
        List<DishIngredientDTO> dishIngredientDTOList = dishIngredientService.create(dishDTO.getIngredients(), newDish.getId());
        newDish.setIngredients(dishIngredientDTOList);

        return newDish;
    }
}
