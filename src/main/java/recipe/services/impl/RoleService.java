package recipe.services.impl;

import recipe.DTO.RoleDTO;

import java.util.List;
import java.util.Set;

public interface RoleService {
    List<String> getRoleNameListFromRoleDTO(Set<RoleDTO> roleDTOs);
}