package recipe.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import recipe.DTO.IngredientDTO;
import recipe.entities.Ingredient;
import recipe.repositories.IngredientRepository;
import recipe.services.IngredientService;

@Service
public class IngredientServiceImpl implements IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public IngredientDTO create(IngredientDTO ingredientDTO) {
        Ingredient newIngredient = new Ingredient(ingredientDTO.getName());
        ingredientRepository.save(newIngredient);
        return modelMapper.map(newIngredient, IngredientDTO.class);
    }
}
