package recipe.services.impl;

import org.modelmapper.ModelMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import recipe.DTO.UserDTO;
import recipe.entities.UserEntity;
import recipe.repositories.UserRepository;
import recipe.services.UserService;

import static recipe.constants.ResponseCodeConstants.*;
import static recipe.utils.ValidateUtil.*;
import static recipe.utils.SecurityUtil.*;
import static recipe.utils.GeneralUtil.*;
import static recipe.constants.RegexConstants.EMAIL_REGEX;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public String validateUser(UserDTO userDTO) {
        // check exists
        if (userRepository.findByUsername(userDTO.getUsername()) != null) {
            return USERNAME_EXISTED;
        }

        if (userRepository.findByEmail(userDTO.getEmail()) != null) {
            return EMAIL_EXISTED;
        }

        // validate email
        String regex = EMAIL_REGEX;

        if (!checkMatchStringByRegex(regex, userDTO.getEmail())) {
            return EMAIL_FAILED_VALIDATING;
        }

        return SUCCESS_MESSAGE;
    }

    @Override
    public UserDTO setDefaultValueWhenCreateUser(UserDTO userDTO) {
        // encode password
        userDTO.setPassword(encodePasswordBCrypt(userDTO.getPassword()));

        // set current timestamp
        userDTO.setCreatedAt(getCurrentTimestamp());

        return userDTO;
    }

    @Override
    public UserDTO findByUsername(String username) {
        UserEntity userEntity = userRepository.findByUsername(username);
        return modelMapper.map(userEntity, UserDTO.class);
    }
}