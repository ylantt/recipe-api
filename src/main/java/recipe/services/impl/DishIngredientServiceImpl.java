package recipe.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import recipe.DTO.DishIngredientDTO;
import recipe.entities.DishIngredient;
import recipe.repositories.DishIngredientRepository;
import recipe.services.DishIngredientService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class DishIngredientServiceImpl implements DishIngredientService {
    @Autowired
    private DishIngredientRepository dishIngredientRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<DishIngredientDTO> create(List<DishIngredientDTO> dishIngredientDTOList, UUID dishId) {
        List<DishIngredientDTO> dishIngredientDTOResult = new ArrayList<DishIngredientDTO>();

        for (DishIngredientDTO dishIngredientDTO : dishIngredientDTOList) {
            DishIngredient newDishIngredient =
                    new DishIngredient( dishIngredientDTO.getIngredientId(),
                                        dishIngredientDTO.getAmount(),
                                        dishIngredientDTO.getUnit());
            newDishIngredient.setDishId(dishId);
            dishIngredientRepository.save(newDishIngredient);
            dishIngredientDTOResult.add(modelMapper.map(newDishIngredient, DishIngredientDTO.class));
        }

        return dishIngredientDTOResult;
    }
}
