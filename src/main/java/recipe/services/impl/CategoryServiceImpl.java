package recipe.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import recipe.DTO.CategoryDTO;
import recipe.entities.Category;
import recipe.repositories.CategoryRepository;
import recipe.services.CategoryService;

import java.util.Optional;
import java.util.UUID;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public CategoryDTO create(CategoryDTO categoryDTO) {
        Category newCategory = new Category(categoryDTO.getName(), categoryDTO.getCateDescription());
        categoryRepository.save(newCategory);
        return modelMapper.map(newCategory, CategoryDTO.class);
    }

    @Override
    public CategoryDTO get(UUID cateId) {
        Optional<Category> category = categoryRepository.findById(cateId);

        return category.map(value -> modelMapper.map(value, CategoryDTO.class)).orElse(null);
    }
}
