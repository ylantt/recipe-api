package recipe.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import recipe.DTO.RoleDTO;
import recipe.DTO.UserDTO;
import recipe.DTO.UserDetailsImpl;
import recipe.utils.JwtTokenUtil;
import recipe.entities.UserEntity;
import recipe.payload.response.LoginResponse;
import recipe.repositories.UserRepositoryCustom;
import recipe.services.AuthService;
import recipe.services.UserService;

import java.util.List;
import java.util.Set;

import static recipe.constants.ResponseCodeConstants.SUCCESS_MESSAGE;
import static recipe.constants.ResponseCodeConstants.SERVER_ERROR;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepositoryCustom userRepositoryCustom;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private RoleService roleService;

    @Override
    public String signup(UserDTO userDTO) {
        String messageResponse = userService.validateUser(userDTO);

        if (messageResponse != SUCCESS_MESSAGE) {
            return messageResponse;
        }

        UserDTO userDTOWithDefaultValue = userService.setDefaultValueWhenCreateUser(userDTO);

        // map from userDto to userEntity
        UserEntity userEntity = modelMapper.map(userDTOWithDefaultValue, UserEntity.class);

        try {
            userRepositoryCustom.saveNew(userEntity);
            return SUCCESS_MESSAGE;
        } catch (Exception e) {
            return SERVER_ERROR;
        }
    }

    @Override
    public LoginResponse signin(UserDTO userDTO) {
        Authentication authentication
                = authenticationManager
                  .authenticate(
                          new UsernamePasswordAuthenticationToken(
                                  userDTO.getUsername(), userDTO.getPassword()));

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        Set<RoleDTO> roleDTOS = userService.findByUsername(userDetails.getUsername()).getRoles();

        List<String> roleList = roleService.getRoleNameListFromRoleDTO(roleDTOS);

        String token = jwtTokenUtil.generateToken(userDetails.getUsername(), roleList);

        LoginResponse loginResponse = modelMapper.map(userDetails, LoginResponse.class);

        loginResponse.setToken(token);

        return loginResponse;
    }

    @Override
    public UserDTO getMe() {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        return userService.findByUsername(username);
    }
}
