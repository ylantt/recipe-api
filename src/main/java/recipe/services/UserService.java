package recipe.services;

import recipe.DTO.UserDTO;

public interface UserService {
    String validateUser(UserDTO userDTO);
    UserDTO setDefaultValueWhenCreateUser(UserDTO userDTO);
    UserDTO findByUsername(String username);
}