package recipe.services;

import recipe.DTO.DishIngredientDTO;

import java.util.List;
import java.util.UUID;

public interface DishIngredientService {
    List<DishIngredientDTO> create(List<DishIngredientDTO> dishIngredientDTO, UUID dishId);
}
