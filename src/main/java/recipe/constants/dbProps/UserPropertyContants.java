package recipe.constants.dbProps;

public final class UserPropertyContants {
    public final static String USER_TABLE_NAME = "User";
    public final static String ID_COLUMN = "id";
    public final static String USERNAME_COLUMN = "username";
    public final static String EMAIL_COLUMN = "email";
    public final static String OWNER_ID_COLUMN = "owner_id";
    public final static String FULL_NAME_COLUMN = "full_name";
    public final static String PASSWORD_COLUMN = "pass";
    public final static String CREATED_AT_COLUMN = "created_at";
    public final static String UPDATED_AT_COLUMN = "updated_at";
    public final static String DELETED_AT_COLUMN = "deleted_at";
}
