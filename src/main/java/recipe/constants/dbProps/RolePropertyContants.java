package recipe.constants.dbProps;

public final class RolePropertyContants {
    public final static String ROLE_TABLE_NAME = "Role";
    public final static String ID_COLUMN = "id";
    public final static String NAME_COLUMN = "role_name";
}
