package recipe.constants.dbProps;

public final class IDStrategyGeneratingConstants {
    public final static String GENERATOR_UUID_NAME = "UUID";
    public final static String STRATEGY_UUID_SOURCE = "org.hibernate.id.UUIDGenerator";
}
