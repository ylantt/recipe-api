package recipe.constants.dbProps;

public final class UserRolePropertyConstants {
    public final static String USER_ROLE_TABLE_NAME = "user_role";
    public final static String USER_ID_COLUMN = "user_id";
    public final static String ROLE_ID_COLUMN = "role_id";
    public final static String ROLES_PROPERTY = "roles";
}
