package recipe.constants;

public final class ResponseCodeConstants {
    public static final String SUCCESS_MESSAGE = "SUCCESS";
    public static final String EMAIL_EXISTED = "EMAIL_EXISTED";
    public static final String USERNAME_EXISTED = "USERNAME_EXISTED";
    public static final String EMAIL_FAILED_VALIDATING = "EMAIL_FAILED_VALIDATING";
    public static final String SERVER_ERROR = "SERVER_ERROR";
}