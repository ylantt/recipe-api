package recipe.constants.routes;

public final class RouteAuthConstants {
    public static final String GENERAL_AUTH_ROUTE = "/api/auth";
    public static final String SIGN_UP_ROUTE = "/signup";
    public static final String SIGN_IN_ROUTE = "/signin";
    public static final String ME_ROUTE = "/me";
}
