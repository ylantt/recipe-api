package recipe.constants.routes;

public final class RouteSecurityConstants {
    public static final String ADMIN_ROUTES = "/api/admin/**";
    public static final String AUTHENTICATION_ROUTES = "/api/auth/**";
    public static final String ME_ROUTE = "/api/auth/me";
    public static final String API_ROUTES = "/api/**";
}
