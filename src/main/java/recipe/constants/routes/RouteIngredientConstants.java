package recipe.constants.routes;

public final class RouteIngredientConstants {
    public static final String GENERAL_INGREDIENT_ROUTE = "/api/ingredients";
}
