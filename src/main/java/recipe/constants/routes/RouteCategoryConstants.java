package recipe.constants.routes;

public final class RouteCategoryConstants {
    public static final String GENERAL_CATEGORY_ROUTE = "/api/categories";
}
