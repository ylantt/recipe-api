package recipe.constants.routes;

public final class RouteDishConstants {
    public static final String GENERAL_DISH_ROUTE = "/api/dishes";
}
