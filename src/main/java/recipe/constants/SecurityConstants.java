package recipe.constants;

public class SecurityConstants {
    public static final String HEADER_STRING = "Authorization";
    public static final String ADMIN_ROLE = "ADMIN";
    public static final String TOKEN_PREFIX = "Bearer ";
}
