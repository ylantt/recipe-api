package recipe.constants.responseMessages.errorResponses;

public final class JWTResponses {
    public static final String JWT_SIGNATURE_INVALID = "JWT_SIGNATURE_INVALID: {}";
    public static final String JWT_TOKEN_INVALID = "JWT_TOKEN_INVALID: {}";
    public static final String TOKEN_EXPIRED = "TOKEN_EXPIRED: {}";
    public static final String JWT_TOKEN_UNSUPPORTED = "JWT_TOKEN_UNSUPPORTED: {}";
    public static final String JWT_CLAIMS_EMPTY = "JWT_CLAIMS_EMPTY: {}";
}
