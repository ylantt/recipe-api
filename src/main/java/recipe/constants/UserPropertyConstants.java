package recipe.constants;

public final class UserPropertyConstants {
    public static String ID_PROPERTY = "id";
    public static String USERNAME_PROPERTY = "username";
    public static String EMAIL_PROPERTY = "email";
    public static String ROLES_PROPERTY = "roles";
}
