package recipe.constants.jsonRequest;

public final class JsonSignupConstants {
    public final static String USERNAME = "username";
    public final static String PASSWORD = "password";
    public final static String FULL_NAME = "fullName";
    public final static String EMAIL = "email";
}
