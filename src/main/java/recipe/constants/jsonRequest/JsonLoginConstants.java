package recipe.constants.jsonRequest;

public final class JsonLoginConstants {
    public final static String USERNAME = "username";
    public final static String PASSWORD = "password";
}
