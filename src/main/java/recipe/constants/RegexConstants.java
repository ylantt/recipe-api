package recipe.constants;

public final class RegexConstants {
    public static String EMAIL_REGEX = "^[A-Za-z0-9+_.-]+@(.+)$";
}
