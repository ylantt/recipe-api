package recipe.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import java.util.UUID;

import static recipe.constants.dbProps.IDStrategyGeneratingConstants.*;
import static recipe.constants.dbProps.UserPropertyContants.*;

import recipe.utils.GeneralUtil;

@Getter
@Setter
@Entity
@Table
@SQLDelete(sql = "UPDATE Dish SET deleted_at = CURRENT_TIMESTAMP() WHERE id=?")
@FilterDef(name = "deletedDishFilter", parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedDishFilter", condition = "deleted = :isDeleted")
public class Dish {
    @Id
    @GeneratedValue(generator = GENERATOR_UUID_NAME)
    @GenericGenerator(
            name = GENERATOR_UUID_NAME,
            strategy = STRATEGY_UUID_SOURCE
    )
    @Column(updatable = false, nullable = false)
    private UUID id;

    @Column(updatable = false, nullable = false)
    private UUID owner;

    private String name;

    private String dishDesc;

    private double avgRating;

    private int totalCmt;

    private String mediaUrl;

    private String mediaDesc;

    private UUID categoryId;

    private Long createdAt;

    private Long updatedAt;

    private Long deletedAt;

    @PrePersist
    protected void onCreate() {
        createdAt = updatedAt = GeneralUtil.getCurrentTimestamp();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedAt = GeneralUtil.getCurrentTimestamp();
    }
}
