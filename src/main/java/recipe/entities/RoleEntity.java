package recipe.entities;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import recipe.enums.RoleEnum;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static recipe.constants.dbProps.RolePropertyContants.*;
import static recipe.constants.dbProps.IDStrategyGeneratingConstants.*;
import static recipe.constants.dbProps.UserRolePropertyConstants.ROLES_PROPERTY;

@Getter
@Setter
@Entity
@Table( name = ROLE_TABLE_NAME)
public class RoleEntity {
    @Id
    @GeneratedValue(generator = GENERATOR_UUID_NAME)
    @GenericGenerator(
            name = GENERATOR_UUID_NAME,
            strategy = STRATEGY_UUID_SOURCE
    )
    @Column(name = ID_COLUMN, updatable = false, nullable = false)
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = NAME_COLUMN)
    private RoleEnum name;

    @ManyToMany(mappedBy = ROLES_PROPERTY)
    private Set<UserEntity> users = new HashSet<UserEntity>();

    public RoleEntity() {
    }

    public RoleEntity(RoleEnum name) {
        this.name = name;
    }
}