package recipe.entities.compositeId;

import java.io.Serializable;
import java.util.UUID;

public class DishIngredientID implements Serializable {
    private UUID dishId;

    private UUID ingredientId;
}
