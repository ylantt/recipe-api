package recipe.entities;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import static recipe.constants.dbProps.UserPropertyContants.*;
import static recipe.constants.dbProps.UserRolePropertyConstants.*;
import static recipe.constants.dbProps.IDStrategyGeneratingConstants.*;

@Getter
@Setter
@Entity
@Table( name = USER_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(columnNames = USERNAME_COLUMN),
                @UniqueConstraint(columnNames = EMAIL_COLUMN)
        }
)
public class UserEntity {
    @Id
    @GeneratedValue(generator = GENERATOR_UUID_NAME)
    @GenericGenerator(
            name = GENERATOR_UUID_NAME,
            strategy = STRATEGY_UUID_SOURCE
    )
    @Column(name = ID_COLUMN, updatable = false, nullable = false)
    private UUID id;

    @Column(name = OWNER_ID_COLUMN)
    private int ownerId;

    @Column(name = USERNAME_COLUMN)
    private String username;

    @Column(name = FULL_NAME_COLUMN)
    private String fullName;

    @Column(name = EMAIL_COLUMN)
    private String email;

    @Column(name = PASSWORD_COLUMN)
    private String password;

    @Column(name = CREATED_AT_COLUMN)
    private long createdAt;

    @Column(name = UPDATED_AT_COLUMN)
    private long updatedAt;

    @Column(name = DELETED_AT_COLUMN)
    private long deletedAt;

    @ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinTable(name = USER_ROLE_TABLE_NAME,
               joinColumns = @JoinColumn(name = USER_ID_COLUMN),
               inverseJoinColumns = @JoinColumn(name = ROLE_ID_COLUMN))
    private Set<RoleEntity> roles = new HashSet<RoleEntity>();

    public void addRole(RoleEntity role) {
        if (!this.roles.contains(role)) {
            this.roles.add(role);
            role.getUsers().add(this);
        }
    }
}