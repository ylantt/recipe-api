package recipe.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.util.UUID;

import static recipe.constants.dbProps.IDStrategyGeneratingConstants.*;

@Getter
@Setter
@Entity
@Table
public class Category {
    @Id
    @GeneratedValue(generator = GENERATOR_UUID_NAME)
    @GenericGenerator(
            name = GENERATOR_UUID_NAME,
            strategy = STRATEGY_UUID_SOURCE
    )
    @Column(updatable = false, nullable = false)
    private UUID id;

    @Column(unique = true)
    private String name;

    private String cateDescription;

    public Category() {
    }

    public Category(String name, String cateDescription) {
        this.name = name;
        this.cateDescription = cateDescription;
    }
}
