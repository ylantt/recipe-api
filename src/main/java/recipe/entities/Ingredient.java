package recipe.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.util.UUID;

import static recipe.constants.dbProps.IDStrategyGeneratingConstants.GENERATOR_UUID_NAME;
import static recipe.constants.dbProps.IDStrategyGeneratingConstants.STRATEGY_UUID_SOURCE;

@Getter
@Setter
@Entity
@Table
public class Ingredient {
    @Id
    @GeneratedValue(generator = GENERATOR_UUID_NAME)
    @GenericGenerator(
            name = GENERATOR_UUID_NAME,
            strategy = STRATEGY_UUID_SOURCE
    )
    @Column(updatable = false, nullable = false)
    private UUID id;

    @Column(unique=true)
    private String name;

    public Ingredient() {}

    public Ingredient(String name) {
        this.name = name;
    }
}
