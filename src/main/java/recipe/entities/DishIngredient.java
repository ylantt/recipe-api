package recipe.entities;

import lombok.Getter;
import lombok.Setter;
import recipe.entities.compositeId.DishIngredientID;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@IdClass(DishIngredientID.class)
@Table
public class DishIngredient {
    @Id
    private UUID dishId;

    @Id
    private UUID ingredientId;

    private int amount;

    private String unit;

    public DishIngredient() {}

    public DishIngredient(UUID ingredientId, int amount, String unit) {
        this.ingredientId = ingredientId;
        this.amount = amount;
        this.unit = unit;
    }
}
